console.log("Hello World");

/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:
// first name
	let givenName = "Miggy";
	console.log("First Name: " + givenName);
// last name
	let familyName = "Atienza";
	console.log("Last Name: " + familyName);
// age
	let age = 29;
	console.log("Age: " + age);
// hobbies
	let hobbies = ["gaming", "drawing", "learning"];
	console.log("Hobbies:");
	console.log(hobbies);
// address
	let address = {
		houseNumber: 1600,
		street: "Pennsylvannia Avenue NW",
		city: "Washington",
		state: "D.C."
	}
	console.log("Work Address:");
	console.log(address);

/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	
// full name
	let fullName = "Steve Rogers";
	console.log("My full name is: " + fullName);
// age
	let currentAge = 40;
	console.log("My current age is: " + currentAge);
// friends 
	let friends = ['Tony', 'Bruce', 'Thor' , 'Natasha', 'Clint', 'Nick'];
	console.log("My Friends are: ")
	console.log(friends);
// profile
	let profile = {

		username: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false,

	}
	console.log("My Full Profile: ")
	console.log(profile);
// bff
	let bestFriend = "Bucky Barnes";
	console.log("My bestfriend is: " + bestFriend);
// location
	const lastLocation = "Arctic Ocean";
	//lastLocation = "Atlantic Ocean";
	console.log("I was found frozen in: " + lastLocation);
